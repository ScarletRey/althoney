var call = document.querySelector(".call-btn");
var callWindow = document.querySelector(".modal-call");
$(function() {
  $('.call-btn').click(function(event) {
    event.preventDefault();
    callWindow.classList.toggle("show");
  });
  $(document).click(function (event) {
      if ($(event.target).closest('.modal-call').length == 0 && $(event.target).closest('.call-btn').length == 0) {
          callWindow.classList.remove("show");
      }
  });
});

$(document).ready(function() {
$('#opis-btn').click(function(event) {
  event.preventDefault();
  $('.itm-otziv').removeClass("show");
  $('#otziv-btn').removeClass("active");
  $('.itm-opisanie').addClass("show");
  $('#opis-btn').addClass("active");
});

$('#otziv-btn').click(function(event) {
  event.preventDefault();
  $('.itm-opisanie').removeClass("show");
  $('#opis-btn').removeClass("active");
  $('.itm-otziv').addClass("show");
  $('#otziv-btn').addClass("active");
});

$('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});
});
