$(document).ready(function() {
  var slides = $(".review-block .review-width").children(".reviewer");
  var width = $(".review-block .review-width").width();
  var widthSlide = $(".review-block .review-width .reviewer").innerWidth();
  var pad = widthSlide - $(".review-block .review-width .reviewer").width();
  var offset = slides.length * widthSlide;
  $(".review-block .review-width").css('width',offset);
  var x = 0;
  var y = width;
  $(".reviews .slidebtn").click(function(){
    event.preventDefault();
    if (width < (offset-768)) {
      width += widthSlide;
      x += widthSlide;
      $(".review-block .review-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
    } else { while (width > y) {
      width -= widthSlide;
      x -= widthSlide;
      $(".review-block .review-width").css("transform","translate3d(-"+x+"px, 0px, 0px)");
    }}
  });

var call = document.querySelector(".call-btn");
var callWindow = document.querySelector(".modal-call");
var order = document.querySelector(".order-btn");
var orderWindow = document.querySelector(".modal-order");
$(function() {
  $('.call-btn').click(function(event) {
    event.preventDefault();
    callWindow.classList.toggle("show");
  });
  $(document).click(function (event) {
      if ($(event.target).closest('.modal-call').length == 0 && $(event.target).closest('.call-btn').length == 0) {
          callWindow.classList.remove("show");
      }
  });
});

$(function() {
  $('.order-btn').click(function(event) {
    event.preventDefault();
    orderWindow.classList.toggle("show");
  });
  $(document).click(function (event) {
      if ($(event.target).closest('.modal-order').length == 0 && $(event.target).closest('.order-btn').length == 0) {
          orderWindow.classList.remove("show");
      }
  });
});
});
